package maps

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestKeys(t *testing.T) {
	dict := map[string]int{
		"a": 0,
		"b": 1,
		"c": 2,
	}
	keys := Keys(dict)
	for _, k := range keys {
		assert.Contains(t, []string{"a", "b", "c"}, k)
	}
}

func TestValues(t *testing.T) {
	dict := map[string]int{
		"a": 0,
		"b": 1,
		"c": 2,
	}
	values := Values(dict)
	for _, v := range values {
		assert.Contains(t, []int{0, 1, 2}, v)
	}
}
