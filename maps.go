package maps

func Keys[M ~map[K]V, K comparable, V any](m M) []K {
	r := make([]K, 0, len(m))
	for k := range m {
		r = append(r, k)
	}
	return r
}

func Values[M ~map[K]V, K comparable, V any](m M) []V {
	r := make([]V, 0, len(m))
	for _, v := range m {
		r = append(r, v)
	}
	return r
}

func Entries[M ~map[K]V, K comparable, V any](m M) []struct {
	Key   K
	Value V
} {
	r := make([]struct {
		Key   K
		Value V
	}, 0, len(m))
	for k, v := range m {
		r = append(r, struct {
			Key   K
			Value V
		}{Key: k, Value: v})
	}
	return r
}

func Merge[M ~map[K]V, K comparable, V any](left M, right M) M {
	for key, rightVal := range right {
		if leftVal, present := left[key]; present {
			//then we don't want to replace it - recurse
			left[key] = any(Merge(any(leftVal).(M), any(rightVal).(M))).(V)
		} else {
			// key not in left so we can just shove it in
			left[key] = rightVal
		}
	}
	return left
}

func Map[M ~map[K]V, K comparable, V any, R any](m M, f func(K, V) R) []R {
	var result []R
	for k, v := range m {
		result = append(result, f(k, v))
	}
	return result
}
